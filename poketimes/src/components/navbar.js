import React from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';

const Navbar = (props) => {


    return (
        <nav className="nav-wrapper red darken-3">
            <div className="container">
                <Link to="/" className="brand-logo">Poke'Times</Link>
                <ul className="right">
                    <li className="waves-effect"><Link to="/">Home</Link></li>
                    <li className="waves-effect"><NavLink to="/about">About</NavLink></li>
                    <li className="waves-effect"><NavLink to="/contact">Contact</NavLink></li>
                </ul>
            </div>
        </nav>
    );
}
export default withRouter(Navbar);