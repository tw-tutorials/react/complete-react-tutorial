import React from 'react';
import Rainbow from '../hoc/rainbow';

const About = () => {


    return (
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Qui vel eos optio iure rem molestias, dolorum enim accusamus soluta tempora hic. Pariatur, labore dolor perferendis voluptatum ratione consequuntur? Corrupti, earum!</p>
        </div>
    )
}

export default Rainbow(About);