import React, { Component } from 'react';
import Ninjas from './components/Ninjas';
import AddNinja from './components/AddNinja';

class App extends Component {
    state = {
        ninjas: [
            { name: 'Ryu', age: 30, belt: 'black', id: 1 },
            { name: 'Yoshi', age: 20, belt: 'green', id: 2 },
            { name: 'Crystal', age: 25, belt: 'pink', id: 3 }
        ]
    }

    addNinja = (ninja) => {
        ninja.id = Math.random();

        let ninjas = [...this.state.ninjas, ninja];
        this.setState({
            ninjas: ninjas
        });
    }
    removeNinja = (id) => {
        let ninjas = this.state.ninjas.filter(ninja => {
            return (ninja.id !== id);
        });
        this.setState({
            ninjas: ninjas
        });
    }


    render() {
        return (
            <div className="App">
                <h1>My first React App!</h1>
                <p>Welcome :)</p>
                <Ninjas ninjas={this.state.ninjas} removeNinja={this.removeNinja}></Ninjas>
                <AddNinja addNinja={this.addNinja}></AddNinja>
            </div>
        );
    }
    componentDidMount() {
        console.log('component mounted');
    }
    componentDidUpdate(prevProps, prevState) {
        console.log('component updated', prevProps, prevState);
    }
}

export default App;
